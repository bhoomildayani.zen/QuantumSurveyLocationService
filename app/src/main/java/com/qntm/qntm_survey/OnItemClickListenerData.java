package com.qntm.qntm_survey;

import android.view.View;

/**
 * Created by admin on 31-08-2017.
 */

public interface OnItemClickListenerData {
    public void onClick(View view, int position);
}
